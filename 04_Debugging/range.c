#include <stdio.h>
#include <stdlib.h>

typedef struct {
    long start;
    long stop;
    long step;
    long current;
} range;

void argparse(int argc, char *argv[], long *start, long *stop, long *step) {
    if (argc <= 1) {
        printf ("range start [stop [step]]");
    } else if (argc == 2) {
        *start = 0;
        *stop = strtol(argv[1], NULL, 10);
        *step = 1;
    } else if (argc >= 3) {
        *start = strtol(argv[1], NULL, 10);
        *stop = strtol(argv[2], NULL, 10);
        *step = 1;
    }
    if (argc >= 4) {
        *step = strtol(argv[3], NULL, 10);
    }
}

void range_init(range *range) {
    range->current = range->start;
}

int range_run(range *range) {
    return range->current < range->stop;
}

void range_next(range *range) {
    range->current += range->step;
}

int range_get(range *range) {
    return range->current;
}

int main(int argc, char *argv[]) {
        range I;
        argparse(argc, argv, &I.start, &I.stop, &I.step);
        for(range_init(&I); range_run(&I); range_next(&I))
                printf("%d\n", range_get(&I));
        return 0;
}
