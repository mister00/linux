set height 0
break 48 if I.current % 5 == 0
commands 1
    echo @@@
    print I.stop
    echo @@@
    print I.start
    echo @@@
    print I.step
    echo @@@
    print I.current
    continue
end
run 1 12 > /dev/null
quit
