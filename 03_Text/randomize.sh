#!/bin/bash

delay=$1

total_chars=0
lines=()
while IFS='\n' read -r line; do
    echo "$line"
    lines+=("$line")
    total_chars=$((total_chars+${#line}))
done

indexes=$(shuf -i 0-$((total_chars-1)))

tput clear
for index in ${indexes[@]}; do
    count=$index
    row=0
    while ((count >= ${#lines[row]})); do
        count=$(($count-${#lines[row]}))
        row=$(($row+1))
    done

    tput cup $row $count
    echo -n "${lines[$row]:$count:1}"

    sleep $delay
done
tput cup $(( $(tput lines) - 1 )) 0
