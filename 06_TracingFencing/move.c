#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <infile> <outfile>\n", argv[0]);
        return 1;
    }

    char *infile = argv[1];
    char *outfile = argv[2];

    FILE *fp_in = fopen(infile, "rb");
    if (fp_in == NULL) {
        perror("Error opening input file");
        return 1;
    }

    FILE *fp_out = fopen(outfile, "wb");
    if (fp_out == NULL) {
        perror("Error opening output file");
        fclose(fp_in);
        return 1;
    }

    int had_error = 0;
    char buffer[1024];
    size_t read_bytes;
    while ((read_bytes = fread(buffer, sizeof(char), sizeof(buffer), fp_in)) > 0 ) {
        if (ferror(fp_in)) {
            perror("Error reading from input file");
            had_error = 1;
            break;
        }
        size_t written_bytes = fwrite(buffer, sizeof(char), read_bytes, fp_out);
        if (written_bytes != read_bytes) {
            fprintf(stderr, "Error writing to output file\n");
            had_error = 1;
            break;
        }
    }
    if (fclose(fp_out) != 0) {
        perror("Error closing output file");
        had_error = 1;
    }

    if (!had_error) {
        if (remove(infile) != 0) {
            perror("Error deleting input file");
            return 1;
        }
    } else {
        if (remove(outfile) != 0) {
            perror("Error recovering the initial state deleting output file");
            return 1;
        }
        return 1;
    }
    fclose(fp_in);
    return 0;
}
