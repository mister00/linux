#define _GNU_SOURCE
#include <stdio.h>
#include <dlfcn.h>
#include <string.h>

int remove(const char *path) {
    if (strstr(path, "PROTECT") != NULL) {
        return 0;
    }
    int (*remove_original)(const char *);
    remove_original = dlsym(RTLD_NEXT, "remove");
    return remove_original(path);
}
