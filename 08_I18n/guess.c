#include <stdio.h>
#include <string.h>
#include <libintl.h>
#include <locale.h>

#define _(STRING) gettext(STRING)

int main() {
    int low = 1;
    int high = 100;
    int guess;
    char answer[4];

    setlocale (LC_ALL, "");
	bindtextdomain ("guess", ".");
	textdomain ("guess");
    
    printf(_("Think of a number from 1 to 100.\n"));
    
    while (low < high) {
        guess = (low + high) / 2;
        
        printf(_("Is the number greater than %d? (yes/no) "), guess);
        char* line = NULL;
        size_t len = 0;
        ssize_t read;
        read = getline(&line, &len, stdin);
        
        if (strcmp(line, _("yes\n")) == 0) {
            low = guess + 1;
        } else if (strcmp(line, _("no\n")) == 0) {
            high = guess;
        } else {
            printf(_("Please write one of the following (yes/no).\n"));
        }
    }
    
    printf(_("Your number: %d\n"), low);
    
    return 0;
}
