#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <rhash.h>
#ifdef USE_READLINE
#include <readline/readline.h>
#endif

void hash(const char* hash_name, const char* input) {
    int hash_id;
    if (!strcmp(hash_name, "MD5")) {
        hash_id = RHASH_MD5;
    } else if (!strcmp(hash_name, "SHA1")) {
        hash_id = RHASH_SHA1;
    } else if (!strcmp(hash_name, "TTH")) {
        hash_id = RHASH_TTH;
    } else {
        fprintf(stderr, "Ошибка: Неизвестный алгоритм хеша\n");
        return;
    }
    unsigned char digest[256];

    int res;
    if (input[0] == '"') {
        res = rhash_msg(hash_id, input+1, strlen(input)-2, digest);
    } else {
        res = rhash_file(hash_id, input, digest);
    }
    if (res < 0) {
        fprintf(stderr, "Ошибка хэширования\n");
        return;
    }

    char result[256];
    if (isupper(hash_name[0])) {
        rhash_print_bytes(result, digest, rhash_get_digest_size(hash_id), RHPR_HEX);
    } else {
        rhash_print_bytes(result, digest, rhash_get_digest_size(hash_id), RHPR_BASE64);
    }
    printf("%s\n", result);
}

int main() {
    char* line = NULL;
    size_t len = 0;

    rhash_library_init();
#ifdef USE_READLINE
    while ((line = readline(NULL)) != NULL) {
#else
    ssize_t read;
    while ((read = getline(&line, &len, stdin)) != -1) {
#endif
        char* hash_name;
        char* file_or_string;
        char* text;
        char hash_result[256];

        hash_name = strtok(line, " \n");
        file_or_string = strtok(NULL, " \n");

        if (hash_name == NULL || file_or_string == NULL) {
            fprintf(stderr, "Ошибка: Не достаточно аргументов\n");
            continue;
        }

        hash(hash_name, file_or_string);
    }

    free(line);
    return 0;
}
