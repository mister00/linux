#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DX 3

char* read_file(FILE *file) {
    long fileLen;
    char *buffer;
    fseek(file, 0, SEEK_END);
    fileLen = ftell(file);
    rewind(file);

    buffer = (char *)malloc(fileLen * sizeof(char));
    if (buffer == NULL) {
        printf("System error\n");
        fclose(file);
        return NULL;
    }

    fread(buffer, 1, fileLen, file);

    fclose(file);
    return buffer;
}

int main(int argc, char** argv) {
    if (argc <= 1) {
        fprintf(stderr, "Enter file name\n");
        return 1;
    }

    FILE *file;
    file = fopen(argv[1], "r");
    if (file == NULL) {
        fprintf(stderr, "Unable to open the file\n");
        return 1;
    }
    char* contents = read_file(file);
    if (contents == NULL) {
        return 1;
    }


    WINDOW *win;
    int c = 0;

    initscr();
    noecho();
    cbreak();
    printw("File: %s", argv[1]);
    refresh();

    win = newwin(LINES-2*DX, COLS-2*DX, DX, DX);
    keypad(win, TRUE);
    scrollok (win, TRUE);

    char* ptr = contents;
    char* ptr2 = contents;
    for (int i = 0; i < LINES-2*DX; i++) {
        while(ptr2[0] != '\n') {
            ptr2++;
        }
        ptr2++;
    }
    bool stop_scrolling = false;
    while((c = wgetch(win)) != 27) {
        if (c == ' ' && !stop_scrolling) {
            while(ptr[0] != '\n') {
                ptr++;
            }
            ptr++;
            while(ptr2[0] != '\n' && ptr2[0] != '\0') {
                ptr2++;
            }
            if (ptr2[0] == '\0') {
                stop_scrolling = true;
            }
            ptr2++;
        }
        char* to_print = (char*)malloc((ptr2 - ptr) * sizeof(char) + 1);
        memcpy(to_print, ptr, ptr2-ptr);
        to_print[ptr2-ptr] = '\0';
        werase(win);
        
        wprintw(win, "%s", to_print);
        wrefresh(win);
        free(to_print);
    }
    endwin();

    free(contents);
    return 0;
}
