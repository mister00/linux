#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>

void esub_ptr(const char *regexp, const char *substitution, const char *string) {
    regex_t regex;
    int ret = regcomp(&regex, regexp, REG_EXTENDED);
    if (ret != 0) {
        size_t errbuf_size = regerror(ret, &regex, NULL, 0);
        char *errbuf = malloc(errbuf_size);
        if (errbuf == NULL) {
            fprintf(stderr, "Системная ошибка");
            regfree(&regex);
            return;
        }
        regerror(ret, &regex, errbuf, errbuf_size);
        fprintf(stderr, "Ошибка в регулярном выражении: %s\n", errbuf);
        free(errbuf);
        regfree(&regex);
        return;
    }

    regmatch_t match[10];
    ret = regexec(&regex, string, 10, match, 0);
    if (ret == REG_NOMATCH) {
        printf("%s\n", string);
        regfree(&regex);
        return;
    } else if (ret != 0) {
        size_t errbuf_size = regerror(ret, &regex, NULL, 0);
        char *errbuf = malloc(errbuf_size);
        if (errbuf == NULL) {
            fprintf(stderr, "Системная ошибка");
            regfree(&regex);
            return;
        }
        regerror(ret, &regex, errbuf, errbuf_size);
        fprintf(stderr, "Ошибка при выполнении замены: %s\n", errbuf);
        free(errbuf);
        regfree(&regex);
        return;
    }
    char *output = malloc(strlen(string) * 10);
    if (output == NULL) {
        fprintf(stderr, "Системная ошибка");
        regfree(&regex);
        return;
    }
    const char *sub_ptr = substitution;
    char *out_ptr = output;
    memcpy(out_ptr, string, match[0].rm_so);
    out_ptr += match[0].rm_so;
    while (sub_ptr[0] != '\0') {
        if (sub_ptr[0] == '\\' && sub_ptr[1] != '\0') {
            if (sub_ptr[1] >= '1' && sub_ptr[1] <= '9') {
                int index = sub_ptr[1] - '0';
                if (match[index].rm_so != -1 && match[index].rm_eo != -1) {
                    size_t len = match[index].rm_eo - match[index].rm_so;
                    memcpy(out_ptr, string + match[index].rm_so, len);
                    out_ptr += len;
                } else {
                    fprintf(stderr, "Ошибка: ссылка на несуществующий карман %c\n", sub_ptr[1]);
                    return;
                }
                sub_ptr += 2;
            } else {
                out_ptr[0] = sub_ptr[1];
                out_ptr++;
                sub_ptr += 2;
            }
        } else {
            out_ptr[0] = sub_ptr[0];
            out_ptr++;
            sub_ptr++;
        }
    }
    size_t len = strlen(string) - match[0].rm_eo;
    memcpy(out_ptr, string + match[0].rm_eo, len);
    out_ptr += len;
    out_ptr[0] = '\0';
    printf("%s\n", output);
    free(output);
    regfree(&regex);
}

int main(int argc, char *argv[]) { 
    if (argc != 4) { 
        fprintf(stderr, "Usage: esub_ptr regexp sub_ptrstitution string\n"); 
        return 1; 
    }
    esub_ptr(argv[1], argv[2], argv[3]);
    return 0;
}
